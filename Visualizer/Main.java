import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
public class Main
{
    public static void main( String[] args )
    {
        ANTLRInputStream inputStream = null;
        String fileName = "default.dot";
        if(args.length > 0){
            System.out.println("Argument found, trying to get filestream");
            try {
                File file = new File(args[0]+".gp");
                FileInputStream fileStream = new FileInputStream(file);
                inputStream = new ANTLRInputStream(fileStream);
            } catch ( IOException e){
                e.printStackTrace();
            }
            fileName = args[0]+".dot";
            GUIPROCESSLexer gpLexer = new GUIPROCESSLexer(inputStream);
            CommonTokenStream commonTokenStream = new CommonTokenStream(gpLexer);
            GUIPROCESSParser gpParser = new GUIPROCESSParser(commonTokenStream);
            GUIPROCESSParser.GuiprocessContext guiprocessContext = gpParser.guiprocess();
            GPVisitor visitor = new GPVisitor();
            String graphviz = visitor.visit(guiprocessContext);
            try{
                PrintWriter output = new PrintWriter(fileName);
                output.println(graphviz);
                output.close();
            } catch ( IOException e){
                e.printStackTrace();
            }
        } else {
            System.out.println("Please support a filename without the GP extension as the first argument");
        }
    }
}
