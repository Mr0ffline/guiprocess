import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;


public class GPVisitor2 extends GUIPROCESSBaseVisitor<String>{

    static String result = "";
    static String graphVizConnections = "";
    static int counter = 0;
    static String previousNode= "'START";
    static String previousNodeType = "start";
    static String sameRankNodes = "{rank=same; START,";
    static int clusterCounter = 1;

    static String graphVizString = "digraph g {\n"+
    "newrank = true;\n"+
    "graph[compound = true, splines=polyline, rankdir=TB];\n"+
    "subgraph cluster0 {\n"+
        "label=\"Start";

    static String keyClusterString = "subgraph clusterKey {label=\"Key\";\n"+
        "GUIAction [shape=house, style=filled, fillcolor = firebrick2, label=\"GUI action\"];\n"+
        "GUIAction2 [shape=house, style=filled, fillcolor = firebrick2, label=\"GUI action\"];\n"+
        "GUIEvent [shape=invhouse, style=filled, fillcolor = darkolivegreen2, label=\"GUI event\"];\n"+
        "GUIEvent2 [shape=invhouse, style=filled, fillcolor = darkolivegreen2, label=\"GUI event\"];\n"+
        "GUITrans [shape=plaintext, label=\"GUI Sequence Transition\"];\n"+
        "GUIEvent -> GUIAction [label=\"Action/Event transistion\"];\n"+
        "GUIInvis [style=invis, shape=invhouse];\n"+
        "GUIEvent -> GUIInvis [style=invis];\n"+
        "GUIAction -> GUITrans [style=invis];\n"+
        "GUIAction2 -> GUIEvent2 [arrowhead=vee];\n"+
        "GUIAction-> GUIAction2 [style=invis];\n"+
        "{rank=same; GUIAction2; GUIEvent2}\n"+
        "{rank=same; GUIEvent; GUIInvis}\n"+
        " }\n";

    private String quoteRemover(String withQuotes){
        return withQuotes.replace("\""," ");
    }

    private String prepareString(String[] withQuotes){
        String multiline = "";
        for (int i = 0;i < withQuotes.length; i++ ) {
            System.out.println(withQuotes[i]);
            String temp = withQuotes[i].replace("\""," ");
            switch (temp){
                case "Mouseclickmain":
                    temp = "Mouseclick main";
                    break;
                case "Mouseclicksecondary":
                    temp = "Mouseclick secondary";
                    break;
                case "Scrollto":
                    temp = "Scroll to";
                    break;
                case "DragHOLDINGshift":
                    temp = "Drag while holding shift";
                    break;
                case "ChangeViewopen":
                    temp = "ChangeView open";
                    break;
                case "ChangeViewclose":
                    temp = "ChangeView close";
                    break;
                case "ChangeViewupdate":
                    temp = "ChangeView update";
                    break;
                case "EditFile":
                    temp = "Edit File";
                    break;
            }
            if(withQuotes[i].trim() != ""){
                if((withQuotes.length-i) > 1 ){
                    multiline =multiline + temp + "\n";
                } else{
                    multiline =multiline + temp;
                }
            }
        }
        return multiline;
    }

    private String addEvent(String label){
        String nodeString= "node"+counter;
        String eventString = "";
        eventString += nodeString+" [shape=invhouse, style=filled, fillcolor = darkolivegreen2, label=\""+label+"\"]; \n";

        if (previousNodeType == "action"){
            graphVizConnections += "node"+(counter-1)+" -> "+ nodeString+ " [arrowhead=vee]; \n";
        } else if(previousNodeType == "start") {
            graphVizConnections += "START -> "+ nodeString+ " [arrowhead=vee]; \n";
        } else {
            eventString += "node"+(counter-1)+" -> "+ nodeString+ "; \n";
        }

        sameRankNodes += nodeString+", ";

        counter++;
        previousNodeType = "event";

        return eventString;
    }

    private String addAction(String label){
        String nodeString = "node"+counter;

        String actionString = nodeString+" [shape=house, style=filled, fillcolor = firebrick2, label=\""+label+"\"]; \n";
        String connection = "node"+(counter-1)+" -> "+ nodeString+ "; \n";

        previousNodeType = "action";
        counter++;
        return actionString+connection;
    }

    @Override
    public String visitGuiprocess(GUIPROCESSParser.GuiprocessContext context)
    {
        String guiProcessString= "";
        if(context.start() != null){
            System.out.println("START FOUND");
            String startString = visitStart(context.start());
            guiProcessString += startString+"\n"+ keyClusterString +
            "\n START-> GUIEvent [style=invis]; \n}\n";
        }

        if(context.guisequence() != null){
            System.out.println("GUI SEQUENCE FOUND");
            String guisequenceString = visitGuisequence(context.guisequence());
            guiProcessString += guisequenceString;
        }

        if(context.end() != null){
            System.out.println("END FOUND");
            String endString = visitEnd(context.end());
            guiProcessString += endString;
        }

        if(context.ref() != null){
            System.out.println("REF FOUND");
            graphVizString += visitRef(context.ref());
        }

        graphVizString += "\";\n";

        sameRankNodes += "END}";
        //System.out.println(" GUI process visited ");

        guiProcessString += sameRankNodes+"\n"+graphVizConnections + " \n }";

        return graphVizString + guiProcessString;
    }

    @Override
    public String visitStart(GUIPROCESSParser.StartContext context)
    {

        //String vizString  = "START [shape=box, label=\" {<here> START | {";

        String vizString  = "START [shape=plaintext, label= < <table cellborder ='0' style='rounded'> <tr> <td> </td> <td> START GUIprocess </td> </tr> <tr>";

        vizString += "<td>Initial view: " + quoteRemover(context.QUOTEDSTRING(2).getText()) + "</td> ";
        vizString += "<td>IDE: " + quoteRemover(context.QUOTEDSTRING(1).getText()) + "</td> ";
        vizString += "<td>Name: " +quoteRemover(context.QUOTEDSTRING(0).getText()) + " </td>";
        vizString += "</tr> </table> >];";

        return vizString;
    }

    @Override
    public String visitEnd(GUIPROCESSParser.EndContext context)
    {
        String endString = "END [shape=doublecircle, style=filled, fillcolor=darkorchid2, label=\"END\"]; \n";

        graphVizConnections += "node"+(counter-1)+ " -> END [arrowhead=vee];";

        return endString;
    }

    @Override
    public String visitGuisequence(GUIPROCESSParser.GuisequenceContext context)
    {
        String sequenceString = "";

        if(context.eventsequence() != null){
        for (GUIPROCESSParser.EventsequenceContext esequence : context.eventsequence()) {
            String esequenceString = "";
            System.out.println("EVENT SEQUENCE FOUND");
            esequenceString = "subgraph cluster"+clusterCounter+" { \n "+
            "label=\"GUI Sequence "+clusterCounter+" ";

            esequenceString += visitEventsequence(esequence);

            esequenceString += "}\n";

            clusterCounter++;
            sequenceString += esequenceString;
            }
        }



        return sequenceString;
    }

    @Override
    public String visitEventsequence(GUIPROCESSParser.EventsequenceContext context)
    {
        String sequenceString = "";
        if (context.ref() != null) {
            System.out.println("FOUND REF");
            sequenceString += visitRef(context.ref());
        }

        sequenceString+= "\n\";";

        if(context.guievent() != null){
            System.out.println("EVENT FOUND");
            sequenceString += visitGuievent(context.guievent());
        }

        if(context.guiaction() != null){
            System.out.println("ACTION FOUND");
            for (GUIPROCESSParser.GuiactionContext action:context.guiaction()) {
                sequenceString += visitGuiaction(action);
            }
        }

        return sequenceString;
    }



    @Override
    public String visitGuievent(GUIPROCESSParser.GuieventContext context)
    {
        String[] multiline = context.getText().split("\"");
        return addEvent(prepareString(multiline));
    }

    @Override
    public String visitGuiaction(GUIPROCESSParser.GuiactionContext context)
    {
        String[] multiline = context.getText().split("\"");
        return addAction(prepareString(multiline));
    }

    @Override
    public String visitRef(GUIPROCESSParser.RefContext context){
        System.out.println("IN REF NOW");
        String refString = "\n";
        if(context.videoRef() != null){
            refString += visitVideoRef(context.videoRef());
        }

        if(context.imgRef() != null){
            refString += visitImgRef(context.imgRef());
        }

        return refString;
    }

    @Override
    public String visitImgRef(GUIPROCESSParser.ImgRefContext context){
        return quoteRemover(context.FILEPATH().getText());
    }

    @Override
    public String visitVideoRef(GUIPROCESSParser.VideoRefContext context){
        String videoRefString = "\n";
        videoRefString += visitVideoRefLocation(context.videoRefLocation());

        if(context.videoRefTime() != null){
            videoRefString += "\n";
            videoRefString += visitVideoRefTime(context.videoRefTime());
        }

        return videoRefString;
    }

    @Override
    public String visitVideoRefLocation(GUIPROCESSParser.VideoRefLocationContext context){
        return quoteRemover(context.getText());
    }

    @Override
    public String visitVideoRefTime(GUIPROCESSParser.VideoRefTimeContext context){
        return quoteRemover(context.getText());
    }
}
