START "Add Outlet" IN " Xcode" ON "Swift project main view"
    => ( Mouseclick main "View menu point"
        -> ChangeView open "View menu" )
    => ( Mouseclick main "Assistant editor"
        -> ChangeView open "Assistant editor submenu" )
    => ( Mouseclick main "Show assistant editor"
        -> ChangeView close "Menu"
        -> ChangeView open "Assistant editor view" )
    => ( Drag HOLDING shift "Button" Drop "Controllerfile"
        -> ChangeView open "Action dialog form" )
    => ( FillForm Pages 1
        -> EditFile "Controllerfile" "Add outlet" )
    => END
