java -cp antlr-4.8-complete.jar org.antlr.v4.Tool GUIPROCESS.g4 -visitor
javac -cp antlr-4.8-complete.jar *.java

java -cp .:antlr-4.8-complete.jar Main AddButton
dot -Tsvg AddButton.dot -o AddButton.svg
dot -Tpng AddButton.dot -o AddButton.png

java -cp .:antlr-4.8-complete.jar Main AddButton2
dot -Tsvg AddButton2.dot -o AddButton2.svg
dot -Tpng AddButton2.dot -o AddButton2.png

java -cp .:antlr-4.8-complete.jar Main AddLabel
dot -Tsvg AddLabel.dot -o AddLabel.svg
dot -Tpng AddLabel.dot -o AddLabel.png

java -cp .:antlr-4.8-complete.jar Main AddListener
dot -Tsvg AddListener.dot -o AddListener.svg
dot -Tpng AddListener.dot -o AddListener.png

java -cp .:antlr-4.8-complete.jar Main AddOutlet
dot -Tsvg AddOutlet.dot -o AddOutlet.svg
dot -Tpng AddOutlet.dot -o AddOutlet.png

java -cp .:antlr-4.8-complete.jar Main AddButtonFromVideo
dot -Tsvg AddButtonFromVideo.dot -o AddButtonFromVideo.svg
dot -Tpng AddButtonFromVideo.dot -o AddButtonFromVideo.png

java -cp .:antlr-4.8-complete.jar Main createProject
dot -Tsvg createProject.dot -o createProject.svg
dot -Tpng createProject.dot -o createProject.png

java -cp .:antlr-4.8-complete.jar Main createProject2
dot -Tsvg createProject2.dot -o createProject2.svg
dot -Tpng createProject2.dot -o createProject2.png
