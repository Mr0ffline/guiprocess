START "Add Label" IN "Xcode" ON "Storyboard"
    => ( Mouseclick main "Show object library"
        -> ChangeView open "Object library" )
    => ( Scroll to "Label element"
        -> ChangeView update "Object library" )
    => ( Drag "Label Element" Drop "Storyboard view"
        -> ChangeView close "Object library"
        -> ChangeView update "Storyboard"
        -> EditFile "Needed files" )
    => END
