START "Create new Project" IN "Xcode" ON "Xcode startup view"
    => ( Mouseclick main "Create new project"
        -> ChangeView open "Create new project form" )
    => ( FillForm Pages 3
        -> ChangeView close "Create new project form"
        -> MakeFile "Filecollection for new project"
        -> ChangeView open "Main project view" )
    => END
