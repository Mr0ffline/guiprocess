START "Add Listener" IN " Xcode" ON "Swift Project Main View"
    => ( Mouseclick main "View"
        -> ChangeView open "View Menu" )
    => ( Mouseclick main "Assistant Editor"
        -> ChangeView open "Assistant Editor Submenu" )
    => ( Mouseclick main "Show Assistant Editor"
        -> ChangeView close "Menu"
        -> ChangeView open "Assistant Editor View" )
    => ( Drag HOLDING shift "Button" Drop "Controllerfile"
        -> ChangeView open "Action Dialog Form" )
    => ( FillForm Pages 1
        -> ChangeView close "Action Dialog Form"
        -> EditFile "Controllerfile" "add Listener"
        -> ChangeView update "Controllerfile" )
    => END
