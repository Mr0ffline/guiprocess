START "Add Button" IN "Xcode" ON "Storyboard"
    => ( Mouseclick main "Show the object library"
        -> ChangeView open "Object library" )
        <=> VIDEOREFERENCE: "https://www.youtube.com/watch?v=giyfrctuya8" 00:13:00 TO 00:15:00
    => ( Scroll to "Button element"
        -> ChangeView update "Object library" )
        <=> VIDEOREFERENCE: "https://www.youtube.com/watch?v=giyfrctuya8" 00:15:00 TO 00:19:00
    => ( Drag "Button element" Drop "Storyboard view"
        -> ChangeView close "Object library"
        -> ChangeView update "Storyboard"
        ->  EditFile "Needed files" )
        <=> VIDEOREFERENCE: "https://www.youtube.com/watch?v=giyfrctuya8" 00:36:00 TO 00:44:00
    => END
        <=> VIDEOREFERENCE: "https://www.youtube.com/watch?v=giyfrctuya8" 00:00:00 TO 00:45:00
