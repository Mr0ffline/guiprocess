grammar GUIPROCESS;

guiprocess: start guisequence end (ref)?;
guisequence: (SEQUENCEMARKER eventsequence)+;
eventsequence: '(' guievent (TRANSITIONMARKER guiaction)+ ')' (ref)?;

guievent: (mouseclick
        | mousehover
        | dragndrop
        | scroll
        | userinput
        | fillform
        | fillformdetailed)
        (ref)?;

guiaction: (fileAction
        | callmethod
        | waitforinput
        | changeview)
        (ref)?;

start: START QUOTEDSTRING IN QUOTEDSTRING ON QUOTEDSTRING;
end: SEQUENCEMARKER END;

mouseclick : MOUSECLICKIDENT  MOUSECLICKTYPE  QUOTEDSTRING;
userinput  : USERINPUTIDENT  INPUTTYPE  QUOTEDSTRING;
scroll     : SCROLLIDENT  DIRECTION ( QUOTEDSTRING)?;
mousehover : MOUSEHOVERIDENT QUOTEDSTRING;
dragndrop  : DRAGIDENT (holdkey)? QUOTEDSTRING  DROPIDENT  QUOTEDSTRING;
fillform   : FILLFORMIDENT  PAGESIDENT  DIGITS;
fillformdetailed   : FILLFORMDETAILIDENT  (userinput)+  confirminput;

fileAction   : FILEACTIONTYPE (QUOTEDSTRING)+;
callmethod   : CALLMETHODIDENT  QUOTEDSTRING;
waitforinput : WAITFORINPUTIDENT;
changeview   : CHANGEVIEWIDENT  VIEWACTION  QUOTEDSTRING;

confirminput: INPUTTYPE;
holdkey:  HOLDING SINGLEKEY;
multiplekey: SINGLEKEY  PLUS  (SINGLEKEY | multiplekey);

ref:  REFMARKER  (videoRef|imgRef);
imgRef: IMAGEREFERENCE COLON FILEPATH;
videoRef : VIDEOREFERENCE COLON videoRefLocation videoRefTime?;
videoRefLocation: URL | FILEPATH;
videoRefTime: TIMEMARKER TO TIMEMARKER;

MOUSECLICKIDENT: 'Mouseclick';
USERINPUTIDENT: 'UserInput';
SCROLLIDENT: 'Scroll';
MOUSEHOVERIDENT: 'Mousehover';
DRAGIDENT: 'Drag';
DROPIDENT: 'Drop';
FILLFORMIDENT: 'FillForm';
FILLFORMDETAILIDENT: 'FillFormDetail';
PAGESIDENT: 'Pages';

FILEACTIONTYPE: 'MakeFile' | 'EditFile';
CALLMETHODIDENT: 'CallMethod';
WAITFORINPUTIDENT: 'WaitForInput';
CHANGEVIEWIDENT: 'ChangeView';

VIEWACTION: 'open'
            | 'close'
            | 'minimize'
            | 'hide'
            | 'show'
            | 'maximize'
            | 'focus'
            | 'update';
SINGLEKEY:  'shift'
            | 'ctrl'
            | 'alt';
MOUSECLICKTYPE :'main'
                |'middle'
                |'secondary' ;
HOLDING: 'HOLDING';
PLUS: '+';

INPUTTYPE: 'Keyboard' | 'Mouse';
DIRECTION:  'up'
            |'down'
            |'left'
            |'right'
            |'to';

START: 'START';
ON: 'ON';
IN: 'IN';
END: 'END';
SEQUENCEMARKER: '=>';
TRANSITIONMARKER: '->';
REFMARKER: '<=>';
COLON: ':';

IMAGEREFERENCE: 'IMAGEREFERENCE';
VIDEOREFERENCE: 'VIDEOREFERENCE';
TO: 'TO';
TIMEMARKER: DIGIT DIGIT COLON DIGIT DIGIT COLON DIGIT DIGIT;

SPACERS:    (' '
            |'\t'
            |'\r'
            |'\n'
            |'\u000C')->skip;

DIGITS: DIGIT+;
DIGIT : ('0'..'9');

FILEPATH: '"' STRINGFRAG'.'STRINGFRAG '"';
URL: '"' 'https://' STRINGFRAG '"';
QUOTEDSTRING :'"' STRINGFRAG '"';

fragment STRINGFRAG: ( '\\' [\\"] | ~[\\"\r\n] )+;

